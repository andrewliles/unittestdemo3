package uk.co.tribalworldwide.stockprice3;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

public class LiveFetcherTest {

	private Fetcher f = new LiveFetcher();
	
	@Test
	public void fetchRawStock() {
		String rawStock = f.fetchRawStock("OMC");
		assertThat(rawStock.length() > 10, is(true));
	}
	
}
