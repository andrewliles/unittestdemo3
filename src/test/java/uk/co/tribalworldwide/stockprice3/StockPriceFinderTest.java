package uk.co.tribalworldwide.stockprice3;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Test;

import uk.co.tribalworldwide.stockprice1.StockPriceFinder;

public class StockPriceFinderTest {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd_HHmm");

	@Test
	public void getStock() throws ParseException {
		Map<String, Object> model = StockPriceFinder.getStock(StockPriceFinder.STOCK_SYMBOL_OMNICOM);
		assertThat(model.size(), is(2));
		assertThat(((Double) model.get(StockPriceFinder.KEY_STOCK_VALUE)).doubleValue(),
				Matchers.closeTo(80.34D, 0.0001D));
		assertThat(((Date) model.get(StockPriceFinder.KEY_STOCK_TIME)), is(SDF.parse("20161019_1602")));
	}

}
