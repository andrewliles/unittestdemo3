package uk.co.tribalworldwide.stockprice3;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Test;

public class SimpleParserTest {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd_HHmm");

	private Parser p = new SimpleParser();
	
	@Test
	public void parseResponse1() throws ParseException {
		Map<String, Object> model = p.parseResponse("\"OMC\",84.96,\"9/27/2016\",\"4:02pm\",+1.09,83.89,85.01,83.65,1040031");
		assertThat(model.size(), is(2));
		assertThat(((Double) model.get(Parser.KEY_STOCK_VALUE)).doubleValue(), Matchers.closeTo(84.96D, 0.0001D));
		assertThat(((Date) model.get(Parser.KEY_STOCK_TIME)), is(SDF.parse("20160927_1602")));
	}

	@Test
	public void parseResponse2() throws ParseException {
		Map<String, Object> model = p.parseResponse("\"MSFT\",708.00,\"10/19/2011\",\"8:22am\",+32.50,698.00,724.50,694.50,3848281");
		assertThat(model.size(), is(2));
		assertThat(((Double) model.get(Parser.KEY_STOCK_VALUE)).doubleValue(), Matchers.closeTo(708D, 0.0001D));
		assertThat(((Date) model.get(Parser.KEY_STOCK_TIME)), is(SDF.parse("20111019_0822")));
	}

	@Test(expected=ParseException.class)
	public void parseResponse3() throws ParseException {
		p.parseResponse("\"APPL\",0.00,\"N/A\",\"N/A\",N/A,N/A,N/A,N/A,N/A");
	}
	
}
