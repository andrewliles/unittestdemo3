package uk.co.tribalworldwide.stockprice1;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.net.URI;

import org.junit.Ignore;
import org.junit.Test;

public class LiveFetcherTest {

	private Fetcher f = new LiveFetcher();
	
	@Test
	@Ignore
	public void getStock() throws Exception {
		String response = f.fetchContent(new URI("http://download.finance.yahoo.com/d/quotes.csv?f=sl1d1t1c1ohgv&e=.csv&s=MSFT"));
		//assertThat(response, is("\"MSFT\",58.03,\"9/28/2016\",\"4:00pm\",+0.08,57.88,58.06,57.67,20537420\n"));
		assertThat(response.startsWith("\"MSFT"), is(true));
	}
}
