package uk.co.tribalworldwide.stockprice1;

import java.io.IOException;
import java.net.URI;

import org.apache.http.client.ClientProtocolException;

public interface Fetcher {

	String fetchContent(URI uri) throws Exception;
	
}
