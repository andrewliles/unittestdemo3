package uk.co.tribalworldwide.stockprice1;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class StockPriceFinder {

	public static final String KEY_STOCK_VALUE = "value";
	public static final String KEY_STOCK_TIME = "time";
	public static final String STOCK_SYMBOL_OMNICOM = "OMC";
	public static final String STOCK_SYMBOL_APPLE = "AAPL";
	public static final String STOCK_SYMBOL_MICROSOFT = "MSFT";

	private static final String STOCK_URL = "http://download.finance.yahoo.com/d/quotes.csv?f=sl1d1t1c1ohgv&e=.csv&s=";
	private static Log log = LogFactory.getLog(StockPriceFinder.class);

	private final Fetcher fetcher;

	public StockPriceFinder(Fetcher fetcher) {
		this.fetcher = fetcher;
	}

	/**
	 * data should be like this:<br/>
	 * <code>	"LBI.AS",1.38,"10/19/2011","3:00am",0.00,1.38,1.38,1.38	</code>
	 */
	private static final Pattern STOCK_DATA_PARSER = Pattern
			.compile("^[^,]+,([^,]+),\"([0-9]{1,2}/[0-9]{1,2}/[0-9]{4})\",\"([0-9]{1,2}:[0-9]{2}[ap]m)\",");

	/**
	 * Returns a Map containing:
	 * <ul>
	 * <li>key {@link #KEY_STOCK_VALUE} a double value of the price in the
	 * traded currency (Euros)</li>
	 * <li>key {@link #KEY_STOCK_TIME} a Date object for when the price applies
	 * </li>
	 * </ul>
	 * 
	 * @return
	 */
	public static Map<String, Object> getStock(String symbol) {
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			HttpClient client = new DefaultHttpClient();
			HttpResponse response = client.execute(new HttpGet(STOCK_URL + symbol));
			String responseBody = EntityUtils.toString(response.getEntity());
			if (response.getStatusLine().getStatusCode() != 200) {
				log.error(responseBody);
			} else {
				log.debug("Stock data: " + responseBody);
				// parse the body
				Matcher m = STOCK_DATA_PARSER.matcher(responseBody);
				if (m.find() & m.groupCount() == 3) {
					double stockValue = Double.parseDouble(m.group(1));
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mma");
					Date stockTime = sdf.parse(m.group(2) + " " + m.group(3));

					model.put(KEY_STOCK_VALUE, stockValue);
					model.put(KEY_STOCK_TIME, stockTime);
				} else {
					log.error("Response did not match Regex: " + responseBody);
				}
			}

		} catch (Exception e) {
			log.error(e);
		}
		return model;
	}

}
