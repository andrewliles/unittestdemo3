package uk.co.tribalworldwide.stockprice1;

import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class LiveFetcher implements Fetcher {

	@Override
	public String fetchContent(URI uri) throws Exception {
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(new HttpGet(uri));
		String responseBody = EntityUtils.toString(response.getEntity());
		if(response.getStatusLine().getStatusCode()!=200) {
			throw new Exception("Cannot fetch data: " + response.getStatusLine());
		}
		else {
			return responseBody;
		}
	}

}
