package uk.co.tribalworldwide.stockprice3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SimpleParser implements Parser {

	/** data should be like this:<br/>
	 * <code>	"LBI.AS",1.38,"10/19/2011","3:00am",0.00,1.38,1.38,1.38	</code>
	 */
	private static final Pattern STOCK_DATA_PARSER = Pattern.compile("^[^,]+,([^,]+),\"([0-9]{1,2}/[0-9]{1,2}/[0-9]{4})\",\"([0-9]{1,2}:[0-9]{2}[ap]m)\",");
	private static Log log = LogFactory.getLog(StockPriceFinder.class);
	
	@Override
	public Map<String, Object> parseResponse(String responseBody) throws ParseException {
		Map<String, Object> model = new HashMap<String, Object>();
		log.debug("Stock data: " + responseBody);
		//parse the body
		Matcher m = STOCK_DATA_PARSER.matcher(responseBody);
		if(m.find() & m.groupCount()==3) {
			double stockValue = Double.parseDouble(m.group(1));
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mma");
			Date stockTime = sdf.parse(m.group(2)+" "+m.group(3));

			model.put(KEY_STOCK_VALUE, stockValue);
			model.put(KEY_STOCK_TIME, stockTime);
		}
		else {
			throw new ParseException("Response did not match Regex: "+ responseBody, 0);
		}
		return model;
	}

}
