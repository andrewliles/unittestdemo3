package uk.co.tribalworldwide.stockprice3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class StockPriceFinder {

	public static final String STOCK_SYMBOL_OMNICOM = "OMC";
	public static final String STOCK_SYMBOL_APPLE = "AAPL";
	public static final String STOCK_SYMBOL_MICROSOFT = "MSFT";

	private static final String STOCK_URL = "http://download.finance.yahoo.com/d/quotes.csv?f=sl1d1t1c1ohgv&e=.csv&s=";
	private static Log log = LogFactory.getLog(StockPriceFinder.class);


	/** Returns a Map containing:
	 *  <ul><li>  key {@link #KEY_STOCK_VALUE} a double value of the price in the traded currency (Euros) </li>
	 *  <li>	  key {@link #KEY_STOCK_TIME} a Date object for when the price applies </li></ul>
	 * @return
	 */
	public static Map<String, Object> getStock(String symbol) {
		String responseBody = new LiveFetcher().fetchRawStock(symbol);
		try {
			return parseResponse(responseBody);
		} catch (ParseException e) {
			log.error(e.getMessage(), e);
			return Collections.emptyMap();
		}
	}

	static Map<String, Object> parseResponse(String responseBody) throws ParseException {
		return new SimpleParser().parseResponse(responseBody);
	}

}
