package uk.co.tribalworldwide.stockprice3;

import java.text.ParseException;
import java.util.Map;

public interface Parser {
	
	String KEY_STOCK_VALUE = "value";
	String KEY_STOCK_TIME = "time";
	
	Map<String, Object> parseResponse(String responseBody) throws ParseException;
	
}
