package uk.co.tribalworldwide.stockprice3;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class LiveFetcher implements Fetcher {

	private static final String STOCK_URL = "http://download.finance.yahoo.com/d/quotes.csv?f=sl1d1t1c1ohgv&e=.csv&s=";
	
	@Override
	public String fetchRawStock(String symbol) {
		try {
			HttpClient client = new DefaultHttpClient();
			HttpResponse response = client.execute(new HttpGet(STOCK_URL+symbol));
			String responseBody = EntityUtils.toString(response.getEntity());
			if(response.getStatusLine().getStatusCode()!=200) {
				throw new RuntimeException("Non successful response: " + response.getStatusLine());
			}
			else {
				return responseBody;
			}

		}
		catch(Exception e) {
			throw new RuntimeException("Non successful response: " + e.getMessage(), e);
		}
	}

}
