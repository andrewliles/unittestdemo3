package uk.co.tribalworldwide.stockprice3;

public interface Fetcher {

	String fetchRawStock(String symbol);
	
}
