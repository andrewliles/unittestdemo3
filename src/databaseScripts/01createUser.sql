create table user 
	(username varchar(20) not null,
	firstname varchar(50),
	lastname varchar(50) not null,
	favouriteStockSymbol varchar(10),
	primary key (username));